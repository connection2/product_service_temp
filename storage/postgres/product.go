package postgres

import (
	pb "temp/genproto/product"

	"github.com/jmoiron/sqlx"
)

type productRepo struct {
	db *sqlx.DB
}

//NewUserRepo ...
func NewProductRepo(db *sqlx.DB) *productRepo {
	return &productRepo{db: db}
}

func (r *productRepo) CreateType(req *pb.Type) (*pb.Type, error) {
	typeRepo := pb.Type{}
	err := r.db.QueryRow(`insert into type(name) values($1) returning id, name`,req.Name).Scan(&typeRepo.Id,&typeRepo.Name)
	if err != nil{
		return &pb.Type{}, err
	 }
	return &typeRepo, nil
}
//
func (r *productRepo) CreateProduct(product *pb.Prodct) (*pb.Prodct, error){
	productRepo := pb.Prodct{}
	err := r.db.QueryRow(`insert into products (name, model, owner_id)
	values ($1, $2, $3) returning id, name, model,
	owner_id`,product.Name, product.Model, product.OwnerId).Scan(&productRepo.Id, &productRepo.Name, &productRepo.Model, &productRepo.OwnerId)
	if err != nil{
		return &pb.Prodct{}, err
	}
return &productRepo, nil
}

func (r *productRepo) GetProduct(ID int64) (*pb.Prodct, error) {
	product := pb.Prodct{}
	err := r.db.QueryRow(`select id, name, model, owner_id from products
	 where id = $1`,ID).Scan(&product.Id, &product.Name, &product.Model, &product.OwnerId)
	if err != nil{
		return &pb.Prodct{}, err
	}

	return &product, nil
}

func (r *productRepo) GetUserProducts(ownerID int64) (*pb.Products, error) {
	rows, err := r.db.Query(`select id, name, model, owner_id from products where owner_id = $1`,ownerID)
	if err != nil{
		return &pb.Products{}, err
	}
	defer rows.Close()
	products := &pb.Products{}
	for rows.Next(){
		product := &pb.Prodct{}
		err := rows.Scan(&product.Id, &product.Name, &product.Model, &product.OwnerId)
		if err != nil{
			return &pb.Products{}, err
		}
		products.Product = append(products.Product, product)
		products.Count++
	}
return products, nil
}
//

func (r *productRepo) CreateCategory(category *pb.Category) (*pb.Category, error) {
	categoryRepo := pb.Category{}
	err := r.db.QueryRow(`insert into category(name, type_id) values($1, $2) 
	returning id, name, type_id`,category.Name, category.TypeId).Scan(&categoryRepo.Id, &categoryRepo.Name, &categoryRepo.TypeId)
	if err != nil{
		return &pb.Category{}, err
	}
	return &categoryRepo, nil
}

func (r *productRepo) GetTypeCategorys(typeID int64) (*pb.Categorys, error){
	rows, err := r.db.Query(`select id, name, type_id from category where type_id = $1`,typeID)
	if err != nil{
		return &pb.Categorys{}, err
	}	
	defer rows.Close()
	categorys := &pb.Categorys{}
	for rows.Next(){
		category := &pb.Category{}
		err := rows.Scan(&category.Id, &category.Name, &category.TypeId)
		if err != nil{
			return &pb.Categorys{}, err
		}
		categorys.Category = append(categorys.Category, category)
	
	}
	return categorys, nil
}
